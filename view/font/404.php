<?php
include_once '../../view/font/include/header.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Education</title>
    <!-- font awesome -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/venobox.css">
    <link rel="stylesheet" href="css/slicknav.css">
    <link href="style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">


    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>


    <!------------------------PAGE-AREA START--------------------->
    <div class="all-page-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="all-page-text">
                        <h1>404</h1>
                        <ul>
                            <li><a href="index.html">home <span><i class="fa fa-angle-right"></i></span></a></li>
                            <li>404</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!------------------------PAGE-AREA END--------------------->

    <!------------------------ERROR-AREA START--------------------->
    <div class="error-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="error-text">
                        <h1>404</h1>
                        <h4>Oops! The Page you are looking for cannot be found.</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br> Accusamus laborum quasi architecto dolore repudiandae <br> minima voluptatibus natus et ut doloribus!</p>
                        <a class="error-btn" href="index.html">Back to home</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!------------------------ERROR-AREA END--------------------->

    <!------------------------QUOTE-AREA START--------------------->
    <div class="quote-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <h2>Great way to get success in finalcial services.</h2>
                </div>
                <div class="col-md-4 col-sm-4">
                    <a href="contact.html" class="quote-btn">Contact us</a>
                </div>
            </div>
        </div>
    </div>
    <!------------------------QUOTE-AREA END--------------------->

    <!------------------------FOOTER-AREA START--------------------->
    <div class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-6">
                    <div class="single-footer-widget">
                        <div class="footer-logo">
                            <h3>Fund Management</h3>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, suscipit iste minus itaque voluptate quod, quo ex, a delectus distinctio sapiente officia! Ex distinctio, ratione odit doloremque dolores quidem ipsum!</p>
                        <div class="footer-social-icon">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-youtube"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                    <!-- .single-footer-widget -->
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-footer-widget">
                        <h4 class="footer-widget-title">Importaint link</h4>
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">about</a></li>
                            <li><a href="#">service</a></li>
                            <li><a href="#">course</a></li>
                            <li><a href="#">testimonial</a></li>
                            <li><a href="#">contact</a></li>
                        </ul>
                    </div>
                    <!-- .single-footer-widget -->
                </div>               
                <div class="col-md-4 col-sm-6">
                    <div class="single-footer-widget">
                        <h4 class="footer-widget-title">contact us</h4>
                        <p><strong>Location</strong> Dhanmondi - 32, Dhaka <br> Dhaka - 1215</p>
                        <p><strong>Email</strong> education@gmail.com
                        </p>
                        <p><strong>Phone</strong> +8801723333608
                        </p>
                    </div>
                    <!-- .single-footer-widget -->
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="copyright">
                        <p>Copyright <span>&copy;</span> 2017, All Right Reserved</p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="design-by">
                        <p>Designed by <span>Imran Hoshain</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!------------------------FOOTER-AREA END--------------------->

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/venobox.min.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/jquery.barfiller.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/main.js"></script>

</body>

</html>