<?php
include_once '../../view/font/include/header.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Signin Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link href="../../assets/font/css/font-awesome.css" rel="stylesheet">
    <link href="../../assets/font/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/font/css/owl.carousel.css">
    <link rel="stylesheet" href="../../assets/font/css/animate.css">
    <link rel="stylesheet" href="../../assets/font/css/venobox.css">
    <link rel="stylesheet" href="../../assets/font/css/slicknav.css">
    <link href="../../assets/font/style.css" rel="stylesheet">
    <link href="../../assets/font/css/responsive.css" rel="stylesheet">


    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">
    <div class="col-sm-4"></div>

    <div class="col-md-4">

    <form class="form-signin">
        <h2 class="form-signin-heading">Login</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus><br>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <div class="checkbox">
            <label>
                <input type="checkbox" value="remember-me"> Remember me
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
        </div>
</div> <!-- /container -->


</body>
</html>
