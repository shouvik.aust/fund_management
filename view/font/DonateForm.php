<?php
include_once '../../view/font/include/header.php';
?>


<div class="col-sm-4"></div>

<div class="col-md-4">
    <h2 align="center">Donate</h2>
    <form ></form>
    <form action="#" method="post">

        <div class="form-group">
            <label >Name</label>
            <input name="name" type="text" class="form-control" id="exampleInputName" aria-describedby="text" placeholder="Enter Full Name">
            <small id="name" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>

        <div class="form-group">
            <label for="inputAddress" class="col-form-label">Address</label>
            <input name="address" type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
        </div>
        <div class="form-group">
            <label for="cellNumber" class="col-form-label">Enter your mobile number</label>
            <input name="mobile" type="tel" class="form-control" id="inputAddress" placeholder="01********">
        </div>
        <div class="form-group">
            <label for="inputAmount" class="">Amount</label>

            <input name="password" type="number" class="form-control" id="inputPassword" placeholder="amount of donation">
        </div>

        <div class="form-group">
            <label for="Skills">Select payment gateway:</label>
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input name="BKash" class="form-check-input" type="checkbox" value="BKash">
                    BKash
                </label>
            </div>
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input name="Rocket" class="form-check-input" type="checkbox" value="Rocket">
                    Rocket
                </label>
            </div>

            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input name="UCash" class="form-check-input" type="checkbox" value="UCash">
                    UCash
                </label>
            </div>
        </div>
        <div class="form-group">
            <label for="inputAmount" class="">Account No.</label>

            <input name="password" type="text" class="form-control" id="inputPassword" placeholder="">
        </div>


        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>

</div>

</body>
</html>