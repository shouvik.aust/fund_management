<?php
session_start();
include_once '../include/header.php';
include_once '../../../vendor/autoload.php';
$admin = new App\Admin\Admin();
//$students = $student->index();

$admin = $admin->view($_GET['id']);

?>

    <div id="page-wrapper" style="min-height: 349px;">
        
        <div class="row">

          <div class="col-lg-6 col-lg-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    The Selected Event Details
                </div>
                <div class="panel-body">
                    <img width="500" src="assets/uploads/<?= $admin['image']?>" alt="image loading...">
                        <h4>Event Name: <?= $admin['name']?></h4>
                        <div class="ratings">
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star-empty"></span>
                        </div><hr>
                        Details:
                        <p>
                            Starting Date: <?= $admin['start_date']?><br>
                            Ending Date: <?= $admin['end_date']?><br>
                            Area: <?= $admin['location']?><br>
                            Paying Methods: <?= $admin['pay_method']?>
                        </p>
                        <hr class="line">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="price">Target: ৳ <?= $admin['target']?> </p>
                            </div>
                        </div>
                


                </div>
                <!-- /.panel-body -->
                <div class="panel-footer ">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-3">
                            <div class="event-footer">
                                <a class="btn btn-danger" href="view/admin/fund/delete.php?id=<?php echo $admin['id']?>">Delete</a>
                                <a href="view/admin/fund/edit.php?id=<?php echo $admin['id']?>" class="btn btn-primary">Edit</a>
                                <a href="view/admin/fund/index.php" class="btn btn-default">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>




        </div>
        <!-- /.row -->
    </div>

<?php
include_once '../include/footer.php';
?>