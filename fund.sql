-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 02, 2017 at 06:17 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php-63`
--

-- --------------------------------------------------------

--
-- Table structure for table `fund`
--

CREATE TABLE `fund` (
  `id` int(11) NOT NULL,
  `target` decimal(10,0) NOT NULL,
  `location` varchar(333) NOT NULL,
  `pay_method` varchar(333) NOT NULL,
  `start_date` varchar(333) NOT NULL,
  `end_date` varchar(333) NOT NULL,
  `image` varchar(333) NOT NULL,
  `name` varchar(333) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fund`
--

INSERT INTO `fund` (`id`, `target`, `location`, `pay_method`, `start_date`, `end_date`, `image`, `name`) VALUES
(9, '1111111', 'Bangladesh', 'Bkash, DBBL, Mkash, SureCash', '1 - December - 2016', '1 - December - 2017', 'e050f531fe.PNG', 'a'),
(10, '2222', 'Bangladesh', 'Bkash, DBBL, Mkash, SureCash', '1 - September - 2016', '1 - September - 2017', 'f3a18f39f4.png', 'Bonna'),
(11, '8742', 'cox bazar', 'Bkash, DBBL', '1 - January - 2016', '1 - January - 2017', 'f11971d5c9.png', 'cyclone');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fund`
--
ALTER TABLE `fund`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fund`
--
ALTER TABLE `fund`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
